.org 0x1352aa4 //balloon
hex{00 06 00 00
11 01 00 01
2f 00 00 00 00 00 80 00
0e 5c 00 b8
0e 5d 00 b8
08 00 00 00
10 2b 00 00
10 05 00 00
0c 00 00 00 80 2e fd 8c
09 00 00 00}

//pointer:1200184
//rom offset:1352c50
//ram offset:0e142c50
//max rom offset:1360000



.org 0x1352a90 //0e142a90
hex{00 04 00 00
08 00 00 00
0c 00 00 00 80 56 2a fc
09 00 00 00}

.org 0x1352afc //80562afc star controller

addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui a0, $8056
lw a0, $2a60 (A0)//pointer to star obj
beq a0, r0, !endc
nop
lw t1, $01b0 (A0)
beq t1, r0, !endc //wait till mario is in range to move
nop
or a1, r0, a0
lui a0, $8036
jal $8029E2F8 //returns f0
lw a0, $1160 (A0)
cvt.w.s f0, f0
mfc1 t9, f0
subiu t9, t9, $0028
bgez t9, !endc //goto end if star is > 30 away
lui v0, $8056
lw v0, $2a60 (V0)//pointer to star obj
lui t0, $8036
lw t0, $1160 (T0)
lw t1, $0188 (T0)
beq t1, r0, !waitmario
lui t4, $00ff
and t4, t1, t4 //bparam2
ori t5, r0, $0010
srlv t4, t4, t5
andi t2, t1, $00ff //bparam4
andi t3, t1, $ff00 //bparam3
ori t5, r0, $0008
srlv t3, t3, t5
mtc1 t2, f4
cvt.s.w f4, f4
swc1 f4, $00b8 (V0) //h speed
mtc1 t3, f4
cvt.s.w f4, f4
mtc1 t4, f6
cvt.s.w f6, f6
mtc1 r0, f8
sub.s f6, f8, f6
add.s f8, f6, f4
swc1 f8, $00b0 (V0) //y speed
lw t9, $00c8 (T0)
sw t9, $00c8 (V0)
beq r0, r0, !endc
!waitmario:
lui a0, $8036
lw a1, $1158 (A0)
jal $8029E2F8 //returns f0
lw a0, $1160 (A0)
cvt.w.s f0, f0
mfc1 t9, f0
subiu t9, t9, $0300
bgez t9, !stop //goto end if mario is > 300 away
lui v0, $8056
lw v0, $2a60 (V0)//pointer to star obj
lui t0, $8036
lw t0, $1160 (T0)
lw t7, $01b0 (T0)
bne t7, r0, !zdir
ori t8, r0, $0028
ori t9, r0, $C000
sw t9, $00c8 (V0)
mtc1 t8, f6
cvt.s.w f6, f6
ori t7, r0, $0001
sw t7, $01b0 (T0)
beq r0, r0, !endc
swc1 f6, $00b8 (V0) //x spd

!zdir:
ori t9, r0, $8000
sw t9, $00c8 (V0)
mtc1 t8, f6
cvt.s.w f6, f6
ori t7, r0, $0000
sw t7, $01b0 (T0)
beq r0, r0, !endc
swc1 f6, $00b8 (V0) //h spd

!stop:
lw v0, $2a60 (V0)//pointer to star obj
sw r0, $00ac (V0) //x
sw r0, $00b0 (V0) //y
sw r0, $00b4 (V0) //z
sw r0, $00b8 (V0) //h spd

!endc:
lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018

.org 0x13529ac //E1429AC star extra function

addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui a0, $8036
lw t0, $1160 (A0)
lw t1, $01b0 (T0)
bne t1, r0, !process
lw a1, $1158 (A0)
jal $8029E2F8 //returns f0
lw a0, $1160 (A0)
cvt.w.s f0, f0
mfc1 t9, f0
subiu t9, t9, $0200
bgez t9, !end //goto end if mario is > 200 away
lui t0, $8036
lw t0, $1160 (T0)
ori t1, r0, $0001
sw t1, $01b0 (T0)
lui at, $8056
sw t0, $2a60 (AT) //pointer to star obj
!process:
jal $802a1308 //sine/cosine
nop
jal $8029F070 //process speed
nop


!end:
lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018

.org 0x1352a30

hex{00 04 00 00
11 01 00 01
0C 00 00 00 80 2A 41 20
0C 00 00 00 80 2F 24 F4
08 00 00 00
0c 00 00 00 80 56 29 AC
0C 00 00 00 80 2F 25 B0
09 00 00 00}
