.org 0x1209cd0
//802da04c:nop = king bob omb no music change
//0x184 = health
//9504c:0C 0C 86 6B
//colpointer:1209cd8
//pointer:35c0204
//romoffset:35d92c8
//ramoffset:0e0092c8
//maxromoffset:
//scale:250

hex{00 09 00 00
2a 00 00 00 0e 00 b1 d0
11 01 00 01
10 4a 00 03
0e 43 7f ff
08 00 00 00
0C 00 00 00 80 38 39 CC
0c 00 00 00 80 40 9d 00
09 00 00 00}

.org 0x1209d00

addiu sp, sp, $ffe8
sw ra, $14 (SP)


lui t0, $8036
lw t0, $1160 (T0)
lw t5, $0154 (T0)
beq t5, r0, !init
lh t1, $11A (T0)
lw t2, $d4 (T0)
addu t2, t2, t1
sw t2, $d4 (T0)
lui a0, $1300
jal $8029F95C //find obj
ori a0, a0, $01F4 //king bob omb
beq v0, r0, !endrotate
lui t0, $8036
lw t0, $1160 (T0)
lw t9, $184 (V0) //health
beq t9, r0, !stop
lw t8, $1b0 (T0)
beq t8, t9, !endrotate
ori t3, r0, $2
lh t1, $11A (T0)
addiu t1, t1, $1
multu t1, t3
mflo t1
sh t1, $11a (T0)
sw t9, $1b0 (T0)
beq r0, r0, !endrotate
nop
!stop:
sh r0, $11a (T0)
beq r0, r0, !endrotate
!init:
ori t1, r0, $140
sh t1, $11A (T0)


!endrotate:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $18
