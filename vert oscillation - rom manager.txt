.org 0x219e54 //13000054
hex{00 04 00 00
08 00 00 00
0c 00 00 00 80 40 f1 00
09 00 00 00}

.org 0x120f100 //8040f100
//makes wave motion by changing y pos of vertices
addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui t0, $8034
lw t2, $b1d8 (T0)
lh t3, $0000 (T2)
andi t3, t3, $00d0
beq t3, r0, !ypos
lui t0, $8034


lui a0, $0E03
jal $80277f50 //seg to vmem=v0
ori a0, a0, $9010//vertices location

!loop:
lh t7, $0006 (V0)
bne t7, r0, !next
lui t0, $8034
lwc1 f0, $b1b4 (T0)//z pos
cvt.w.s f0, f0
mfc1 t1, f0
lui t0, $8036
lw t0, $1160 (T0)
lwc1 f2, $0204 (T0) //y pos
cvt.w.s f2, f2
mfc1 t2, f2
lh t8, $0004 (V0) //z
subu t8, t8, t1
lh t9, $0002 (V0) //y
subu t9, t9, t2
multu t8, t8
mflo t8
multu t9, t9
mflo t9
addu t7, t8, t9 //distance squared
mtc1 t7, f12
cvt.s.w f12, f12
sqrt.s f4, f12 //f4 = distance of vert from mario
lw t2, $0154 (T0)
mtc1 t2, f0
cvt.s.w f0, f0
lui t3, $3ec8 //ang frequency
mtc1 t3, f8
mul.s f0, f0, f8 //w*t
lui t3, $3d02 //.0318=wave number
mtc1 t3, f2
mul.s f12, f4, f2//wavenum times distance
swc1 f4, $0200 (T0)
sub.s f12, f0, f12
mfc1 t9, f12
blez t9, !next
sw t2, $0008 (SP)
jal $80325480 //takes f12 sin(wt-kr) returns f0
sw v0, $0004 (SP)
lw v0, $0004 (SP)
lh t9, $0000 (V0) //x
mtc1 t9, f2
cvt.s.w f2, f2//y value in float


lui t0, $4416 //600=amplitude
mtc1 t0, f4
lui t1, $8036
lw t1, $1160 (T1)
lwc1 f16, $0200 (T1) //distance from mario
cvt.w.s f22, f16
mfc1 t7, f22
beq t7, r0, !amplitude
nop
div.s f4, f4, f16 //spacial damping
!amplitude:
mul.s f6, f4, f0 //amplitude times sine function
add.s f8, f2, f6
cvt.w.s f8, f8
mfc1 t9, f8
sh t9, $0000 (V0) //x
!next:
addiu v0, v0, $0010 //next vert
lui v1, $8045
ori v1, v1, $aff0
beq v0, v1, !end
nop
beq r0, r0, !loop
nop

!ypos:
lwc1 f0, $b1b0 (T0)//y pos
lui t1, $8036
lw t1, $1160 (T1)
swc1 f0, $0204 (T1)

!end:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $0018