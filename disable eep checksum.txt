// loadEeprom function
.orga 0x349dc
addiu sp, sp, -0x30
sw ra, 0x1c (sp)

lui at, 0x8034
sb r0, 0xb4a5 (at) // set value at 0x8033b4a5 to 0 (false)
sb r0, 0xb4a6 (at) // set value at 0x8033b4a6 to 0 (false)

la a0, 0x80207700
jal 0x80324570 // bzero function
addiu a1, r0, 0x200

la a0, 0x80207700
jal 0x80279174 // copy eeprom to 0x80207700
addiu a1, r0, 0x200

lw ra, 0x1c (sp)
jr ra
addiu sp, sp, 0x30



.orga 0x34840
addiu sp, sp, -0x18
sw ra, 0x14 (sp)
sw a0, 0x18 (sp)

lb t6, 0x8033b4a6
beqz t6, @@skipfilesave // branch to end if 0x8033b4a6(bool shouldsavefiledata) is 0(false)
nop

lw t4, 0x18 (sp)
addiu t5, r0, 0x70
mult t5, t4 // t5 = filenumber * 0x70
mflo t5 // get result from mult
la t6, 0x80207700
la a0, 0x8033af78 // a0 = pointer to message queue (0x8033af78)
addiu a1, r0, (0x70 / 8) // a1 = (0x70 / 8) = 0x0E
mult a1, t4 // a1 = 0xe * filenumber, used to find address in eeprom.
mflo a1 // get result from mult
addu a2, t5, t6 // a2 = pointer in ram to copy to eeprom
jal 0x803247d0 // oseepromlongwrite, writes data to the eeprom
addiu a3, r0, 0x70 // a3 = number of bytes to save

sb r0, 0x8034b4a6 // set value at 0x8033b4a6 to 0 (false)

@@skipfilesave:
jal 0x802794a0 // call savemenudata() function
nop

lw ra, 0x14 (sp) // end of function
jr ra
addiu sp, sp, 0x18


// savemenudata function
.orga 0x344a0
addiu sp, sp, -0x18
sw ra, 0x14 (sp)

la t6, 0x8033b4a5
beqz t6, @@skipmenusave // branch to end if 0x8033b4a5(bool shouldsavemenudata) is 0(false)
nop

la a0, 0x8033af78 // a0 = pointer to message queue (0x8033af78)
addiu a1, r0, (0x1c0 / 8) // a1 = (0x1c0 / 8) = 0x38
la a2, (0x80207700 + 0x1c0) // 0x802078C0, pointer in ram to copy to eeprom
jal 0x803247d0 // oseepromlongwrite, writes data to the eeprom
addiu a3, r0, 0x40 // a3 = number of bytes to save

sb r0, 0x8033b4a5 // set value at 0x8033b4a5 to 0 (false)

@@skipmenusave:
lw ra, 0x14 (sp) // end of function
jr ra
addiu sp, sp, 0x18