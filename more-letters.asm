; NUM_CHARS configures the number of characters to load
; Lower this number and repatch if SM64 gets unstable or crashes
; 1=J, 2=Q, 3=V, 4=X, 5=Z, 6=!, 7=!!, 8=?, 9=&, 10=%, 11=Circle, 12=Key
.definelabel NUM_CHARS, 11

.definelabel SEG02_BASE, 0x00803156
.definelabel SEG02_END,  0x0081BB64

; Add missing letters to Segment 02

.orga SEG02_END
.incbin "more-letters.bin"

; Insert segment-offset pointers to the new characters

.orga 0x0080A8A2 ; J
.dw 0x02018A10

.orga 0x0080A8BE ; Q
.dw 0x02018C10

.orga 0x0080A8D2 ; V
.dw 0x02018E10

.orga 0x0080A8DA ; X
.dw 0x02019010

.orga 0x0080A8E2 ; Z, !, !!, ?, &, %
.dw 0x02019210, 0x02019410, 0x02019610, 0x02019810, 0x02019A10, 0x02019C10

.orga 0x0080A92E ; Circle, Key
.dw 0x02019E10, 0x0201A010

; Update Segment 02 loader

.definelabel segment_load_raw, 0x8027868C

.orga 0x00003AC0

LI  A1, SEG02_BASE
LI  A2, SEG02_END + (NUM_CHARS * 0x200)
JAL segment_load_raw
ORI A0, R0, 0x02