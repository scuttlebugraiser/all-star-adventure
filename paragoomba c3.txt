.org 0x1d75c58//E065C58
hex{00 04 00 00
11 01 00 01
2f 00 00 00 00 00 80 00} //set interaction, bounced/dmg
hex{0e 5c 00 b8
0e 5d 00 b8} //col sphere
hex{10 3e 00 02} //dmg to mario
hex{08 00 00 00}
hex{10 2b 00 00} //enable general interaction
hex{10 05 00 00} //enable interaction w/ mario
hex{0c 00 00 00 80 2E FD 8C} //moving up and down
hex{09 00 00 00}

//rom offset:1d69d60
//ram offset:E059D60
//pointer:1d0025c
//max ram offset:1e60000