//balloon

.org 0x120d000 //8040d000

hex{00 04 00 00
11 01 00 01
2f 00 00 00 00 00 80 00} //set interaction, bounced/dmg
hex{0e 5c 00 b8
0e 5d 00 b8} //col sphere
hex{08 00 00 00}
hex{10 2b 00 00} //enable general interaction
hex{10 05 00 00} //enable interaction w/ mario
hex{0c 00 00 00 80 2E FD 8C} //moving up and down
hex{09 00 00 00}

//import:balloon
//pointer:26a0204
//colpointer:N/A
//romoffset:2736ac0
//ramoffset:E086Ac0
//maxromoffset:2800000
//scale:150


//rotating platform
.org 0x120d030
//col pointer:120d03c
hex{00 09 00 00
11 01 00 01
2a 00 00 00 0E 08 CC d0
08 00 00 00
0f 13 04 00
10 24 04 00
0c 00 00 00 80 38 39 cc
09 00 00 00}

//import:rotating platform
//pointer:26a020c
//colpointer:120d03c
//romoffset:2739e28
//ramoffset:e089e28
//maxromoffset:2800000
//scale:300



//obj mario hangs from

.org 0x120d058

hex{00 04 00 00
11 01 00 01
2f 00 00 00 00 00 00 01}
hex{0e 5c 00 40
0e 5d 00 40} //col sphere
hex{08 00 00 00}
hex{10 2b 00 00} //enable general interaction
hex{10 05 00 00} //enable interaction w/ mario
hex{0c 00 00 00 80 40 d0 8c} //moving in a circle
hex{0f 13 01 00
09 00 00 00}

//import:hook/thing to hang from
//pointer:26a1410
//colpointer:N/A
//romoffset:273d180
//ramoffset:E08D180
//maxromoffset:2800000
//scale:125




addiu sp, sp, $ffe8
sw ra, $0014 (SP)


lui t0, $8036
lw t0, $1160 (T0)
lw t9, $0188 (T0)
andi t8, t9, $ffff
lw t1, $00c8 (T0)
addu t1, t1, t8
sw t1, $00c8 (T0)
hex{00 19 cc 82} //srl t9, t9, 18
mtc1 t9, f2
cvt.s.w f2, f2
swc1 f2, $00b8 (T0)

jal $802a1308 //sine/cosine
nop
jal $8029f070 //process speed
nop


lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018

.org 0x120d0dc

hex{00 09 00 00
11 01 00 01
2a 00 00 00 0E 09 53 50
08 00 00 00
0c 00 00 00 80 38 39 cc
09 00 00 00}

//import:platform in water
//pointer:26a022c
//colpointer:120d0e8
//romoffset:2741688
//ramoffset:E091688
//maxromoffset:2800000
//scale:300

.org 0x120bcc0
hex{00 09 00 00
11 01 00 01
2a 00 00 00 08 01 2D 70
32 00 00 c8
10 1a 00 01
08 00 00 00
0c 00 00 00 80 38 39 cc
09 00 00 00}


