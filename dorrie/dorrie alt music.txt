.org 0x120a6b0 //8040a6b0
hex{00 04 00 00
0c 00 00 00 80 40 a6 c0
0a 00 00 00}

addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui a0, $8036
lw a1, $1158 (A0)
jal $8029E27C
lw a0, $1160 (A0)
cvt.w.s f0, f0
mfc1 t9, f0
subiu t9, t9, $200
bgez t9, !end
lui a0, $40
jal $8029F95C
ori a0, a0, $6fa0
sh r0, $74 (V0)
or a0, r0, r0
jal $80320544 //play music
ori a1, r0, $43


!end:
lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018


.org 0x120a710 //8040a710
hex{00 04 00 00
08 00 00 00
0c 00 00 00 80 40 a7 24
09 00 00 00}

addiu sp, sp, $ffe8
sw ra, $0014 (SP)

lui t0, $8034
lw t1, $b180 (T0)
ori t2, r0, $1308//reading sign
bne t1, t2, !endpipespawner
lui a0, $8036
lw a0, $1160 (A0)
lui a2, $1300
ori a2, a2, $07a0 //warp pipe
jal $8029edcc //spawn obj
ori a1, r0, $16 //warp pipe
lui t0, $000b
sw t0, $0188 (V0)
lui a0, $8036
lw a0, $1160 (A0)
sh r0, $74 (A0)


!endpipespawner:
lw ra, $0014 (SP)
jr ra
addiu sp, sp, $0018