.org 0x1203f70

hex{00 04 00 00}
hex{08 00 00 00}
hex{0c 00 00 00 80 40 2c 00}
hex{09 00 00 00}

.org 0x1202c00

lui t0, $8034
lw t9, $b1d8 (T0)
lh t9, $0000 (T9)
ori t8, r0, $000d
beq t8, t9, !end
lwc1 f4, $b1b0 (T0)
swc1 f4, $b22c (T0)
lwc1 f0, $b1c8 (T0) //x spd
lwc1 f2, $b1cc (T0) //y spd
ori t7, r0, $0003
mtc1 t7, f1
cvt.s.w f1, f1
div.s f4, f0, f1
div.s f6, f2, f1
add.s f4, f0, f4
add.s f6, f2, f6
add.s f4, f0, f4
add.s f6, f2, f6
!ends:
swc1 f4, $b1c8 (T0) //x spd
swc1 f6, $b1cc (T0) //y spd
!end:
jr ra
nop