.org 0x2db9800 //E029800

hex{00 04 00 00}
hex{08 00 00 00}
hex{0c 00 00 00 80 44 98 14}
hex{09 00 00 00}


.org 0x2db9814 //E029814 / 80 44 98 14

addiu sp, sp, $ffe8
sw ra, $14 (SP)

lui a0, $1300
jal $8029F95C
addiu a0, a0, $472c //goomba
bne v0, r0, !end
nop

lui t0, $461c
ori t0, t0, $e800 // x
mtc1 t0, f12
lui t0, $442c //y
mtc1 t0, f14
lui a2, $c4ed 
jal $802F2B88 //spawn star
ori a2, a2, $e000//z
lui t0, $0400
sw t0, $0188 (V0) //set star number
lui t0, $8036
lw t0, $1160 (T0)
sw r0, $0074 (T0)

!end:
lw ra, $14 (SP)
jr ra
addiu sp, sp, $0018